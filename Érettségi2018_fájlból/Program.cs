﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Érettségi2018_fájlból
{
    
        struct Tadat
        {
            public int mikor; //percekben
            public int ki;    //azonosító
            public bool állapot; //be => igaz
        }
       
    class Program
    {
        const int maxáthaladás = 1000;
        const int maxszemély = 100;
        static int hányadik = 0;
        static Tadat[] társalgó = new Tadat[maxáthaladás];
        static int[] hányszormentát = new int[maxszemély + 1];
        static int személy;
        static void beolvasas()
        {
            string[] tmp = new string[3];               
            foreach ( string be in File.ReadAllLines("ajto.txt")) 
                //beolvassa az egész fájlt egy szövegtömbbe
            {
                    if (be.Trim() != "") //lehet a végén üres sor, később kivételkezeléssel
                    {
                        tmp = be.Split(); //
                        társalgó[hányadik].mikor = 60 * (Convert.ToInt32(tmp[0]) - 9) + Convert.ToInt32(tmp[1]);
                        társalgó[hányadik].ki = Convert.ToInt32(tmp[2]);
                        társalgó[hányadik++].állapot = tmp[3] == "be";
                    }
            };
        }
        static void első_utolsó()
        {
            int első = társalgó[0].ki;
            int utolsó = hányadik;
            while (társalgó[utolsó--].állapot) ;
            Console.WriteLine("2. feladat");
            Console.WriteLine("Az első belépő: {0}",első);
            Console.WriteLine("Az utolsó belépő: {0}", utolsó);
        }
        static void kihanyszor()
        {           
            for (int sorszám=0; sorszám<hányadik;sorszám++)
            {
                hányszormentát[társalgó[sorszám].ki]++;
            }
            Console.WriteLine("3. feladat");
            for (int i = 1; i <= maxszemély; i++)
            {
                if (hányszormentát[i]!=0)
                    Console.Write(hányszormentát[i].ToString() + " "); //később fileba
            }
            Console.WriteLine();
        }
        static void bentmaradtak()
        {   //előző feladat feltölti kihányszormentát tömböt
            Console.WriteLine("4. feladat");
            Console.Write("A végén a társalgóban voltak: ");
            for (int i = 1; i <= maxszemély; i++)
            {
                if (hányszormentát[i] % 2 == 1 )  //többször ment be, tehát bent van
                    Console.Write(i.ToString() + " "); //később fileba
            }
            Console.WriteLine();         
        }
      
        static void legtobben()
        {
            int maxmikor = társalgó[0].mikor; //egy ember legalább van bent
            int[] éppenhányanvannakbent = new int[6 * 60 + 1];
            éppenhányanvannakbent[társalgó[0].mikor] = 1; //az első ember biztos befelé megy
            for (int i = 1; i < hányadik; i++)
            {
                if (társalgó[i - 1].mikor != társalgó[i].mikor)
                {
                    éppenhányanvannakbent[társalgó[i].mikor] = éppenhányanvannakbent[társalgó[i].mikor];
                }
                if (társalgó[i].állapot)
                {
                    ++éppenhányanvannakbent[társalgó[i].mikor]; //jött még valaki
                    if (éppenhányanvannakbent[maxmikor] < éppenhányanvannakbent[társalgó[i].mikor])
                        maxmikor = társalgó[i].mikor;
                }
                else
                {
                    --éppenhányanvannakbent[társalgó[i].mikor]; //valaki elment
                }              
            }
            Console.WriteLine("5. feladat");
            Console.WriteLine("Például {0}-kor voltak legtöbben a teremben",(9+maxmikor/60)+":"+maxmikor%60);
        }
        static void személybeolvasás()
        {
            Console.WriteLine("6. feladat");
            Console.WriteLine("Adjon meg a személy azonosítóját!");
            személy=Convert.ToInt16(Console.ReadLine());
        }
        static void mettőlmeddig()
        {
            Console.WriteLine("7. feladat");
            for (int i = 0; i < hányadik; i++)
            {
                if (társalgó[i].ki == személy)
                {
                    if (társalgó[i].állapot)
                    {
                        Console.Write((9 + társalgó[i].mikor / 60) + ":" + társalgó[i].mikor % 60 + "-");
                    }
                    else
                    {
                        Console.WriteLine((9 + társalgó[i].mikor / 60) + ":" + társalgó[i].mikor % 60 );
                    }
                }
            }
            if (hányszormentát[személy] % 2 == 1) Console.WriteLine(); 
        }
        static void összesenmennyit()
        {
            int mennyit = 0;
            int be = 0;
            for (int i = 0; i < hányadik; i++)
            {
                if ( társalgó[i].ki == személy )
                {
                    if ( társalgó[i].állapot )
                    {
                        be = társalgó[i].mikor;
                    }
                    else
                    {
                        mennyit += társalgó[i].mikor - be + 1;
                    }
                }
            }
            if (hányszormentát[személy] % 2 == 1) mennyit += 15 * 60;
            Console.WriteLine("8. feladat");
            Console.WriteLine("A {0} személy összesen {1} percet volt bent, a megfigyelés ideje alatt", személy, mennyit);
        }

        static void Main(string[] args)
        {
            beolvasas();
            első_utolsó();
            kihanyszor();
            bentmaradtak();           
            legtobben();
            személybeolvasás();
            mettőlmeddig();
            összesenmennyit();
        }
        }
}
